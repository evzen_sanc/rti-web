unit RC_sender_unit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, xmldom, XMLIntf, StdCtrls, msxmldom, XMLDoc, ComCtrls, Grids, EncdDecd, 
  IdCoderMIME, ScktComp;

type
  TMainWindow = class(TForm)
    ServerGroupBox: TGroupBox;
    ServerAddress: TEdit;
    ServerAddressLabel: TLabel;
    ServerPortLabel: TLabel;
    ServerPort: TEdit;
    ResponseCardBox: TGroupBox;
    Address: TEdit;
    Name: TEdit;
    Synonym: TEdit;
    Latitude: TEdit;
    Longitude: TEdit;
    Remark: TRichEdit;
    SynonymLabel: TLabel;
    NameLabel: TLabel;
    AddressLabel: TLabel;
    LatitudeLabel: TLabel;
    LongitudeLabel: TLabel;
    RemarkLabel: TLabel;
    GroupBox1: TGroupBox;
    Contacts: TStringGrid;
    GroupBox2: TGroupBox;
    Signals: TStringGrid;
    GroupBox3: TGroupBox;
    Pictures: TStringGrid;
    Submit: TButton;
    StatusBar: TStatusBar;
    SAC: TEdit;
    SACLabel: TLabel;
    SubmitBlock: TButton;
    ClientSocket: TClientSocket;
    procedure FormCreate(Sender: TObject);
    procedure SubmitClick(Sender: TObject);
    procedure SubmitBlockClick(Sender: TObject);
    procedure PicturesSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    procedure BuildXMLCard(action: string);
    procedure SendXMLCard(action: string);    
    { Private declarations }
  public
    { Public declarations }
  end;

function GetWinCount: dword; stdcall;
  
var
  MainWindow: TMainWindow;

implementation

{$R *.dfm}

function GetWinCount; external kernel32 name 'GetTickCount';


procedure TMainWindow.FormCreate(Sender: TObject);
begin
     Contacts.Cells[0,0]:='Name';
     Contacts.Cells[1,0]:='Position';
     Contacts.Cells[2,0]:='Phone';
     Contacts.Cells[0,1]:='Mister X';
     Contacts.Cells[1,1]:='Director';
     Contacts.Cells[2,1]:='+123456789';
     Contacts.Cells[0,2]:='Mister Y';
     Contacts.Cells[1,2]:='Security Manager';
     Contacts.Cells[2,2]:='+198765432';
     Contacts.Cells[0,3]:='Mister Z';
     Contacts.Cells[1,3]:='Secretary';
     Contacts.Cells[2,3]:='+165498732';

     Signals.Cells[0,0]:='Event';
     Signals.Cells[1,0]:='Timestamp';
     Signals.Cells[2,0]:='ext1';
     Signals.Cells[3,0]:='ext2';
     Signals.Cells[4,0]:='ext3';     
     Signals.Cells[0,1]:='Window broken';
     Signals.Cells[1,1]:='1234567898';
     Signals.Cells[2,1]:='5076175000';
     Signals.Cells[3,1]:='1506896700';
     Signals.Cells[4,1]:='220'; 
     Signals.Cells[0,2]:='Bulglary';
     Signals.Cells[1,2]:='1234567899';
     Signals.Cells[2,2]:='741';
     Signals.Cells[3,2]:='03';
     Signals.Cells[4,2]:=''; 

     Pictures.Cells[0,0]:='File Name';
     Pictures.Cells[1,0]:='Path';
     Pictures.Cells[0,1]:='Demo picture 1';
     Pictures.Cells[1,1]:='demo1.jpg';
     Pictures.Cells[0,2]:='Demo picture 2';
     Pictures.Cells[1,2]:='demo2.jpg';

     StatusBar.Panels[0].Text:='Ready';
end;

procedure TMainWindow.PicturesSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var
   openDialog : TOpenDialog;
begin
     openDialog := TOpenDialog.Create(self);
     openDialog.InitialDir := GetCurrentDir;
     openDialog.Options := [ofFileMustExist];
     openDialog.Filter :=
         'Compatible Types|*.jpg;*.pdf';
     openDialog.FilterIndex := 2;
     if openDialog.Execute then 
     begin
         Pictures.Cells[0, ARow] :=   ExtractFileName(openDialog.FileName);
         Pictures.Cells[1, ARow] :=   openDialog.FileName;         
    
     end;
     openDialog.Free;
end;

procedure TMainWindow.SubmitBlockClick(Sender: TObject);
begin
     StatusBar.Panels[0].Text:='Block request is sending. Please wait...';    
      BuildXmlCard('block');
      
      SendXmlCard('block');
      //StatusBar.Panels[0].Text:='ResponseCard-block.xml has been sent';
end;

procedure TMainWindow.SubmitClick(Sender: TObject);
begin
     StatusBar.Panels[0].Text:='Store request is sending. Please wait...';
     BuildXmlCard('store');
     
     SendXmlCard('store');     
     //StatusBar.Panels[0].Text:='ResponseCard-store.xml has been sent';
end;


procedure TMainWindow.SendXMLCard(action: string);
var receive_frame: string;
start_wait: integer;
Stream : TWinSocketStream;
FStream : TFileStream;
finish : byte;
response: IXMLDocument;
statusInfo: IXMLNode;
sacCode: IXMLNode;
begin
     finish := 6;
     ClientSocket.Host := ServerAddress.Text;
     ClientSocket.Port := StrToInt(ServerPort.Text);
     try
          ClientSocket.Open;
          with ClientSocket.Socket do
          begin
               FStream := TFileStream.Create('ResponseCard-'+action+'.xml', fmOpenRead);
               Stream := TWinSocketStream.Create(ClientSocket.Socket, 5000); // 5 seconds or 5000 milliseconds
               try
               
               
               //send_frame := LoadFileToStr();
               //SetLength(Buffer, 1024); // 100 bytes buffer size

               Stream.CopyFrom(FStream, FStream.Size) ;
               Stream.Write(finish, 1);
              // while (FStream.ReadBuffer( buffer, 1024)>0) do
               //begin
               ///  Stream.WriteBuffer(buffer, Length(buffer));
               //end;
               
               start_wait:=GetWinCount+1;
               while (ReceiveLength<10) and (start_wait+30000>GetWinCount) do
               begin
                    Sleep(1000);
               end;
               receive_frame := ReceiveText;
               finally
                      Stream.Free;
                      FStream.Free;
               end;
               
          end;
     except
     on E: Exception do
        receive_frame:='<response><status code="0">Connection problem</status></response>';
     end;
     ClientSocket.Close;
     //ShowMessage(receive_frame);
     response := LoadXMLData(receive_frame);
     statusInfo := response.DocumentElement.ChildNodes['status'];
     sacCode := response.DocumentElement.ChildNodes['sac'];
     if (statusInfo.Text <> '') then
     begin
        StatusBar.Panels[0].Text := statusInfo.Text;
     end;
     if (sacCode.Text <> '') then
     begin
        Sac.Text := sacCode.Text;
     end;
     
     
     
end;

procedure TMainWindow.BuildXMLCard(action: string);
Var
  i : integer;
  XML : IXMLDOCUMENT;
  RootNode, CardNode, CurNode, CurChild : IXMLNODE;
  istream: TMemoryStream;
  ostream: TMemoryStream;
  filebuf: string;
begin
  XML := NewXMLDocument;
  XML.Encoding := 'utf-8';
  XML.Options := [doNodeAutoIndent]; // looks better in Editor ;)
  RootNode := XML.AddChild('request');
  RootNode.Attributes['version']:='1.0';
  
  if action = 'store' then
  begin
       CurNode := RootNode.AddChild('operation');
       CurNode.Attributes['code']:='1';
       CardNode := RootNode.AddChild('card');

  
       CurNode := CardNode.AddChild('synonym');
       CurNode.Text := Synonym.Text;
       CurNode := CardNode.AddChild('name');
       CurNode.Text := Name.Text;
       CurNode := CardNode.AddChild('location');
       CurNode.AddChild('address').Text := Address.Text;
       CurChild := CurNode.AddChild('gps');
       CurChild.Attributes['latitude'] := Latitude.Text;
       CurChild.Attributes['longitude'] := Longitude.Text;
       CurNode := CardNode.AddChild('remark');

       for i := 0 to Remark.Lines.Count-1 do
       begin
           CurNode.Text := CurNode.Text + Remark.Lines[i];
       end;

       CurNode := CardNode.AddChild('contacts');
       for i := 1 to Contacts.RowCount - 1 do
       begin
            if Contacts.Cells[2, i] <> '' then
                begin
                     CurChild := CurNode.AddChild('contact');
                     CurChild.Attributes['name']:= Contacts.Cells[0, i];
                     CurChild.Attributes['position']:= Contacts.Cells[1, i];
                     CurChild.Text:= Contacts.Cells[2, i];
                end;
            end;
            CurNode := CardNode.AddChild('signals');
            for i := 1 to Signals.RowCount - 1 do
            begin
                 if Signals.Cells[0, i] <> '' then
                 begin
                 CurChild := CurNode.AddChild('signal');
                 CurChild.Attributes['time']:= Signals.Cells[1, i];
                 CurChild.AddChild('description').Text:= Signals.Cells[0, i];
                 if Signals.Cells[2, i] <> '' then
                    CurChild.AddChild('ext1').Text:= Signals.Cells[2, i];
                 if Signals.Cells[3, i] <> '' then
                    CurChild.AddChild('ext2').Text:= Signals.Cells[3, i];
                 if Signals.Cells[4, i] <> '' then
                    CurChild.AddChild('ext3').Text:= Signals.Cells[4, i];
            end;
       end;  
       CurNode := CardNode.AddChild('pictures');
       for i := 1 to Pictures.RowCount - 1 do
       begin
            if (ExtractFileExt(Pictures.Cells[1, i]) = '.jpg') or (ExtractFileExt(Pictures.Cells[1, i]) ='.pdf') then
            begin
                 istream := TMemoryStream.Create;
                 ostream := TMemoryStream.Create;
                 try
                    istream.LoadFromFile(Pictures.Cells[1, i]);
                    CurChild := CurNode.AddChild('picture');
                    CurChild.Attributes['filename']:= Pictures.Cells[0, i];
                    if (ExtractFileExt(Pictures.Cells[1, i]) = '.jpg') then
                       CurChild.Attributes['content-type']:= 'image/jpg';
                    if (ExtractFileExt(Pictures.Cells[1, i]) = '.pdf') then
                       CurChild.Attributes['content-type']:= 'application/pdf';                  
                    EncodeStream(istream, ostream);
                    SetString(filebuf, PChar(ostream.Memory), ostream.Size div SizeOf(Char));
                    CurChild.Text:=filebuf;
                 finally
                    istream.Free;
                    ostream.Free;
                 end;
            end;
       end;
  end
  else
  begin
       CurNode := RootNode.AddChild('operation');
       CurNode.Attributes['code']:='2';
       RootNode.AddChild('sac').Text:=SAC.Text;
  end;
  XML.SaveToFile('ResponseCard-'+action+'.xml');

   
end;

end.
