object MainWindow: TMainWindow
  Left = 0
  Top = 0
  Caption = 'Response Card Sender'
  ClientHeight = 467
  ClientWidth = 712
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object SACLabel: TLabel
    Left = 13
    Top = 416
    Width = 20
    Height = 13
    Caption = 'SAC'
  end
  object ServerGroupBox: TGroupBox
    Left = 8
    Top = 8
    Width = 321
    Height = 73
    Caption = 'Server Information'
    TabOrder = 0
    object ServerAddressLabel: TLabel
      Left = 16
      Top = 35
      Width = 74
      Height = 13
      Caption = 'Server Address'
    end
    object ServerPortLabel: TLabel
      Left = 241
      Top = 35
      Width = 20
      Height = 13
      Caption = 'Port'
    end
    object ServerAddress: TEdit
      Left = 96
      Top = 32
      Width = 129
      Height = 21
      TabOrder = 0
      Text = '62.209.227.90'
    end
    object ServerPort: TEdit
      Left = 265
      Top = 32
      Width = 40
      Height = 21
      TabOrder = 1
      Text = '53499'
    end
  end
  object ResponseCardBox: TGroupBox
    Left = 8
    Top = 87
    Width = 321
    Height = 320
    Caption = 'Response Card Info'
    TabOrder = 1
    object SynonymLabel: TLabel
      Left = 16
      Top = 24
      Width = 44
      Height = 13
      Caption = 'Synonym'
    end
    object NameLabel: TLabel
      Left = 168
      Top = 24
      Width = 69
      Height = 13
      Caption = 'Object'#39's Name'
    end
    object AddressLabel: TLabel
      Left = 16
      Top = 74
      Width = 39
      Height = 13
      Caption = 'Address'
    end
    object LatitudeLabel: TLabel
      Left = 16
      Top = 122
      Width = 39
      Height = 13
      Caption = 'Latitude'
    end
    object LongitudeLabel: TLabel
      Left = 168
      Top = 122
      Width = 47
      Height = 13
      Caption = 'Longitude'
    end
    object RemarkLabel: TLabel
      Left = 16
      Top = 173
      Width = 61
      Height = 13
      Caption = 'RemarkLabel'
    end
    object Address: TEdit
      Left = 16
      Top = 93
      Width = 289
      Height = 21
      TabOrder = 0
      Text = 'Svatopluka Cecha 59, Jablonec nad Nisou, 466 02 '
    end
    object Name: TEdit
      Left = 168
      Top = 43
      Width = 137
      Height = 21
      TabOrder = 1
      Text = 'Penzion Apartman'
    end
    object Synonym: TEdit
      Left = 16
      Top = 43
      Width = 137
      Height = 21
      TabOrder = 2
      Text = '2020'
    end
    object Latitude: TEdit
      Left = 16
      Top = 141
      Width = 137
      Height = 21
      TabOrder = 3
      Text = '50.730101'
    end
    object Longitude: TEdit
      Left = 168
      Top = 141
      Width = 137
      Height = 21
      TabOrder = 4
      Text = '15.178441'
    end
    object Remark: TRichEdit
      Left = 16
      Top = 192
      Width = 289
      Height = 113
      Lines.Strings = (
        'Remark')
      PlainText = True
      TabOrder = 5
    end
  end
  object GroupBox1: TGroupBox
    Left = 335
    Top = 8
    Width = 370
    Height = 129
    Caption = 'Contacts'
    TabOrder = 2
    object Contacts: TStringGrid
      Left = 16
      Top = 24
      Width = 337
      Height = 92
      ColCount = 3
      DefaultColWidth = 100
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 4
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goAlwaysShowEditor]
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 335
    Top = 143
    Width = 370
    Height = 129
    Caption = 'Signals'
    TabOrder = 3
    object Signals: TStringGrid
      Left = 16
      Top = 24
      Width = 337
      Height = 92
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 4
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goAlwaysShowEditor]
      TabOrder = 0
    end
  end
  object GroupBox3: TGroupBox
    Left = 335
    Top = 278
    Width = 370
    Height = 129
    Caption = 'Pictures'
    TabOrder = 4
    object Pictures: TStringGrid
      Left = 16
      Top = 24
      Width = 337
      Height = 92
      ColCount = 2
      DefaultColWidth = 100
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 4
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goAlwaysShowEditor]
      TabOrder = 0
      OnSelectCell = PicturesSelectCell
    end
  end
  object Submit: TButton
    Left = 630
    Top = 413
    Width = 75
    Height = 25
    Caption = 'Store'
    TabOrder = 5
    OnClick = SubmitClick
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 448
    Width = 712
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object SAC: TEdit
    Left = 39
    Top = 413
    Width = 121
    Height = 21
    TabOrder = 7
    Text = '12345678'
  end
  object SubmitBlock: TButton
    Left = 166
    Top = 413
    Width = 75
    Height = 25
    Caption = 'Block'
    TabOrder = 8
    OnClick = SubmitBlockClick
  end
  object ClientSocket: TClientSocket
    Active = False
    ClientType = ctBlocking
    Port = 0
    Left = 592
    Top = 416
  end
end
