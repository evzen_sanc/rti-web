program RC_sender;

uses
  Forms,
  RC_sender_unit in 'RC_sender_unit.pas' {MainWindow};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Response Card Test Sender';
  Application.CreateForm(TMainWindow, MainWindow);
  Application.Run;
end.
