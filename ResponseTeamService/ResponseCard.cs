//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ResponseTeamService
{
    using System;
    using System.Collections.Generic;
    
    public partial class ResponseCard
    {
        public ResponseCard()
        {
            this.Contact = new HashSet<Contact>();
            this.Picture = new HashSet<Picture>();
            this.Signal = new HashSet<Signal>();
        }
    
        public int Id { get; set; }
        public string Sac { get; set; }
        public string Synonym { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Nullable<decimal> Latitude { get; set; }
        public Nullable<decimal> Longitude { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> BlockedAt { get; set; }
        public Nullable<bool> Enabled { get; set; }
        public string Remark { get; set; }
    
        public virtual ICollection<Contact> Contact { get; set; }
        public virtual ICollection<Picture> Picture { get; set; }
        public virtual ICollection<Signal> Signal { get; set; }
    }
}
