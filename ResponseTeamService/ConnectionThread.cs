﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml.Linq;
using System.IO;
using log4net;
using log4net.Config;
using System.Globalization;

namespace ResponseTeamService
{
    class ConnectionThread
    {
        string sac;

        TcpListener threadListener;
        public static readonly ILog log = LogManager.GetLogger(typeof(ConnectionThread));

        /// <summary>
        /// Constructor of connection thread for handling client's request
        /// </summary>
        /// <param name="lis">Pointer to TCP Listener</param>
        public ConnectionThread(TcpListener lis)
        {

            threadListener = lis;
            Thread newthread = new Thread(new ThreadStart(HandleConnection));
            newthread.Start();
        }

        /// <summary>
        /// Method for accepting client data,sending response and closing connection
        /// </summary>
        public void HandleConnection()
        {
            bool done = false;
            char[] trimer = {'\0'};
            byte[] data = new byte[1024];

            StringWriter stream = new StringWriter();
            string card = "";
            
            TcpClient client = threadListener.AcceptTcpClient();
            IPEndPoint endPoint = (IPEndPoint) client.Client.RemoteEndPoint;
            IPAddress ipAddress = endPoint.Address;
            int port = endPoint.Port;
            log.InfoFormat("Client connection accepted from {0}:{1}", ipAddress.ToString(), port.ToString());

            NetworkStream ns = client.GetStream();

            

            while (true)
            {
                
                data = new byte[1024];
                int recv = ns.Read(data, 0, data.Length);
                //log.Info("READ "+recv + "bytes. LAST ["+data[recv-1].ToString()+"]");
                if (recv > 0)
                {
                    if (data[recv - 1] == 6)
                    {
                        data[recv - 1] = 0;
                        done = true;
                    }
                    card += System.Text.Encoding.UTF8.GetString(data).TrimEnd(trimer);
                    

                    if (done)
                        break;

                }
                else
                    break;
                //ns.Write(data, 0, recv);
            }
            log.Info("Parsing XML");


            XDocument response = HandleRequest(card);
            log.Info("Responsed");
            log.InfoFormat("Responsed: {0}", response.Descendants().Where(s => s.Name == "status").FirstOrDefault().Value.ToString());
            data = Encoding.ASCII.GetBytes(response.ToString());
            
            ns.Write(data, 0, data.Length);
            ns.Close();
            client.Close();
        }

        /// <summary>
        /// Method for generating common response XML structure
        /// </summary>
        /// <param name="status_code">Code of state</param>
        /// <param name="status_text">State message</param>
        /// <returns>Response XML Structure</returns>
        public XDocument StartResponse(int status_code, string status_text)
        {
            XDocument xdoc = new XDocument(
                new XElement("response",
                    new XElement("status", new XAttribute("code", status_code.ToString()), status_text)
                )
            );
            return xdoc;
        }

        /// <summary>
        /// Method for finding out what to do with request and preparing response
        /// </summary>
        /// <param name="req">Text Request</param>
        /// <returns>Response XML Structure</returns>
        public XDocument HandleRequest(string req)
        {
            XDocument response = StartResponse(400, "Bad Request");
            try
            { 
                XDocument xdoc = XDocument.Parse(req);
                var action = xdoc.Descendants().Where(n => n.Name == "operation").FirstOrDefault();

                if (action.Attribute("code").Value == "1") //store
                {
                    log.Info("Handling Store Card Request");
                    response = StoreCard(xdoc);
                }
                else if (action.Attribute("code").Value == "2")
                {
                    string bsac = xdoc.Descendants().Where(n => n.Name == "sac").FirstOrDefault().Value.ToString();
                    log.Info("Handling Block Card Request");
                    response = BlockCard(bsac);
                }
            }
            catch (Exception e)
            {
                log.ErrorFormat("{0} Exception caught while parsing XML", e);
            }

            return response;
        }

        /// <summary>
        /// Method for storing card in DB
        /// </summary>
        /// <param name="xdoc">Request XML structure</param>
        /// <returns>Response XML structure</returns>
        public XDocument StoreCard(XDocument xdoc)
        {
            using (RTEntities db = new RTEntities())
            {
                var xcard = xdoc.Descendants().Where(n => n.Name == "card").FirstOrDefault();
                ResponseCard dbcard = new ResponseCard
                {
                    Synonym = (string)xcard.Element("synonym").Value.ToString(),
                    Name = (string)xcard.Element("name").Value.ToString(),
                    //Address = (string)c.Element("Address").Value,
                    Remark = (string)xcard.Element("remark").Value.ToString()
                };
                //var location = xdoc.Descendants().Where(n => n.Name == "location").FirstOrDefault();
                dbcard.Address = xcard.Element("location").Element("address").Value.ToString();
                dbcard.Latitude = decimal.Parse(xcard.Element("location").Element("gps").Attribute("latitude").Value.ToString(), CultureInfo.InvariantCulture);
                dbcard.Longitude = decimal.Parse(xcard.Element("location").Element("gps").Attribute("longitude").Value.ToString(), CultureInfo.InvariantCulture);
                dbcard.CreatedAt = DateTime.Now;
                dbcard.Enabled = true;
                dbcard.Sac = getSac();
                
                var rc = db.Set<ResponseCard>();
                rc.Add(dbcard);
                db.SaveChanges();
                log.InfoFormat("Card Stored with id={0} and sac={1}", dbcard.Id.ToString(), dbcard.Sac);

                var xcontacts = xdoc.Descendants().Where(n => n.Name == "contact").ToList();
                var cont = db.Set<Contact>();
                for (var i = 0; i < xcontacts.Count; i++)
                {
                    cont.Add(new Contact
                    {
                        CardId = dbcard.Id,
                        Name = xcontacts[i].Attribute("name").Value.ToString(),
                        Position = xcontacts[i].Attribute("position").Value.ToString(),
                        Phone = xcontacts[i].Value.ToString()
                    });
                }
                
                var xalarm = xdoc.Descendants().Where(n => n.Name == "signal").ToList();
                var alarm = db.Set<Signal>();
                for (var i = 0; i < xalarm.Count; i++)
                {
                    alarm.Add(new Signal
                    {
                        CardId = dbcard.Id,
                        Ext1 = xalarm[i].Elements("ext1").FirstOrDefault().Value.ToString(),
                        Ext2 = xalarm[i].Elements("ext2").FirstOrDefault().Value.ToString(),
                        //Ext3 = xalarm[i].Elements("ext3").FirstOrDefault().Value.ToString(),
                        //Ext4 = xalarm[i].Elements("ext4").FirstOrDefault().Value.ToString(),
                        //Ext5 = xalarm[i].Elements("ext5").FirstOrDefault().Value.ToString(),
                        Description = xalarm[i].Elements("description").FirstOrDefault().Value.ToString(),
                        OccuredAt = new System.DateTime(1970, 1, 1).AddSeconds(
                            long.Parse(xalarm[i].Attribute("time").Value.ToString()) * 1.0)
                    });
                }
                
                var xpictures = xdoc.Descendants().Where(n => n.Name == "picture").ToList();
                var pic = db.Set<Picture>();
                for (var i = 0; i < xpictures.Count; i++)
                {
                    pic.Add(new Picture
                    {
                        CardId = dbcard.Id,
                        Filename = xpictures[i].Attribute("filename").Value.ToString(),
                        ContentType = xpictures[i].Attribute("content-type").Value.ToString(),
                        Content = Convert.FromBase64String(xpictures[i].Value.ToString())

                    });
                }

                db.SaveChanges();

            }

            XDocument response = StartResponse(200, "OK");
            response.Elements("response").FirstOrDefault().Add(new XElement("sac", sac));

            return response;

        }

        /// <summary>
        /// Method for blocking card
        /// </summary>
        /// <param name="bsac">SAC code for blocking</param>
        /// <returns>Response XML Structure</returns>
        public XDocument BlockCard(string bsac)
        {
            XDocument response = StartResponse(200, "OK");
            using (RTEntities db = new RTEntities())
            {
                ResponseCard bcard = new ResponseCard();
                bcard = db.ResponseCard.Where(rcard => (rcard.Enabled == true && rcard.Sac == bsac)).FirstOrDefault();
                if (bcard.Id>0)
                {
                    bcard.Enabled = false;
                    bcard.BlockedAt = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    response = StartResponse(400, "Bad Request");
                }
            }
            return response;
        }

        /// <summary>
        /// Method for getting free SAC
        /// </summary>
        /// <returns>SAC code</returns>
        public string getSac()
        {
            log.Info("Begin new SAC");
            bool done = false;
            char[] sym = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9'};
            Random r = new Random();
            sac = "";
            do
            {
                for (int i = 0; i < 8; i++)
                    sac += sym[r.Next(sym.Count())];

                using (RTEntities db = new RTEntities())
                {
                    done = !(db.ResponseCard.Any(
                        rcard => 
                            (
                                (rcard.Enabled == true && rcard.Sac == sac) 
                            )
                        )
                    );
                    
                }

            } while (done != true);
            log.Info("End new SAC");
            return sac;
        }
    }
}
