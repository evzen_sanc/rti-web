﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Collections;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using ResponseTeamService;
using log4net;
using log4net.Config;





namespace ResponseTeamService
{
    public partial class Service : ServiceBase
    {
        public static readonly ILog log = LogManager.GetLogger(typeof(Service));
        /// <summary>
        /// Constructor of service class.
        /// </summary>
        public Service()
        {
            InitializeComponent();
            log.Info("RT Service has been initialized");
        }

        /// <summary>
        /// Starting operations
        /// </summary>
        /// <param name="args">program arguments</param>
        protected override void OnStart(string[] args)
        {
            log.Info("RT Service started");
            ThreadPool.QueueUserWorkItem(new WaitCallback(ServiceWorkerThread)); 
        }

        /// <summary>
        /// Worker thread of TCP daemon
        /// </summary>
        /// <param name="state">system param</param>
        private void ServiceWorkerThread(object state) 
        {
            string ListenIP = Properties.Settings.Default.IP;
            int ListenPort = Properties.Settings.Default.Port;

            try
            {

                IPAddress host = IPAddress.Parse(ListenIP);

                TcpListener client = new TcpListener(host, ListenPort);
                client.Start();
            

                log.Info("Waiting for clients");
                while (true)
                {
                    while (!client.Pending())
                    {
                        Thread.Sleep(1000);
                    }
                    log.Info("Client connected. Starting new thread");
                    ConnectionThread newconnection = new ConnectionThread(client);
                }
            }
            catch (Exception e)
            {
                log.InfoFormat("{0} Exception caught while starting TCP Server", e);
            }
            //using (RTEntities entity = new RTEntities())
            //{
            
            //byte[] buffer = File.ReadAllBytes("C:\\Develop\\JablonetPRO\\ResponseTeam\\ResponseConsole\\card\\demo.jpg");
            //var pics = entity.Set<Contact>();
            //pics.Add(new Contact { CardId = 1, Name="Started", Position="1", Phone="2" });
            

            //entity.SaveChanges();
            //}
            //string[] lines = { "First line", "Second line", "Third line" };
            //System.IO.File.WriteAllLines(@"C:\WriteLines.txt", lines);
        }
        /// <summary>
        /// Stopping operations
        /// </summary>
        protected override void OnStop()
        {
            log.Info("Service has been stoped");
        }
    }
}
