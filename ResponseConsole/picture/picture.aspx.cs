using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using ResponseConsole;
using System.Collections;
using System.Data;

using log4net;
using log4net.Config;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Drawing2D;


public partial class PicturePage : System.Web.UI.Page
{
    public static readonly ILog log = LogManager.GetLogger(typeof(PicturePage));
    protected void Page_Load(object sender, EventArgs e)
    {
        log4net.Config.DOMConfigurator.Configure();
        string sac=Request["sac"];
        int pic = Convert.ToInt32( Request["pic"]);
        
        if (sac == null)
        {
            log.Error("[SAC:NULL,PIC:"+pic+"] Picture without SAC requested");
            Response.Redirect("../");
        }

        Regex rgx = new Regex(@"^[a-z0-9]{8}$", RegexOptions.IgnoreCase);
        MatchCollection matches = rgx.Matches(sac);
        if (matches.Count == 0)
        {
            log.Error("[SAC:" + sac + ",PIC:"+pic+"] Invalid request for picture");
            Response.Redirect("../?e=400");
        }

        using (RTEntities entity = new RTEntities())
        {
            int CardId;
            List<ResponseCard> card = new List<ResponseCard>();
            List<Picture> pictures = new List<Picture>();

            card = entity.ResponseCard.Where(rcard => (rcard.Enabled == true && rcard.Sac == sac)).ToList();
            if (card.Count == 0)
            {
                log.Error("[SAC:" + sac + ",PIC:" + pic + "] Picture requested for missed or blocked card");
                Response.Redirect("/?e=404");
            }
            CardId = card[0].Id;
            pictures = entity.Picture.Where(p => (p.CardId == CardId && p.Id==pic)).ToList();
            if (pictures.Count == 0)
                Response.Redirect("../?e=404");

            

            Response.Clear();
            Response.ContentType = pictures[0].ContentType;
            if (pictures[0].ContentType =="image/jpg" && Request["preview"]=="1")
            {
                int width=100;
                int height=100;
                TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                Bitmap myImage = (Bitmap)tc.ConvertFrom(pictures[0].Content );
                if (myImage.Width > myImage.Height)
                    height = (100 * myImage.Height) / myImage.Width;
                else
                    width = (100 * myImage.Width) / myImage.Height; 
                Bitmap preBitmap = new Bitmap(myImage, new Size(width, height));
                ImageConverter converter = new ImageConverter();
                
                Response.BinaryWrite((byte[])converter.ConvertTo(preBitmap, typeof(byte[])));

            }
            else
                Response.BinaryWrite(pictures[0].Content);
            Response.End();
            log.Info("[SAC:" + sac + ",PIC:" + pic + "] Picture '" + pictures[0].Filename + "' has been shown");
            Response.Close();
            
                
            
        }
    }

 

}