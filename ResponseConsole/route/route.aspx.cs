using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using ResponseConsole;
using System.Collections;
using System.Data;
using log4net;
using log4net.Config;


public partial class RoutePage : System.Web.UI.Page
{
    public static readonly ILog log = LogManager.GetLogger(typeof(RoutePage));
    protected void Page_Load(object sender, EventArgs e)
    {
        log4net.Config.DOMConfigurator.Configure();
        string sac=Request["sac"];
        string googleurl="http://maps.google.com/maps?daddr=";
        string saddr = Request["saddr"];
        if (sac == null)
        {
            log.Error("[SAC:NULL] Route without SAC requested");
            Response.Redirect("../");
        }

        Regex rgx = new Regex(@"^[a-z0-9]{8}$", RegexOptions.IgnoreCase);
        MatchCollection matches = rgx.Matches(sac);
        if (matches.Count == 0)
        {
            log.Error("[SAC:" + sac + "] Invalid request for route");
            Response.Redirect("../?e=400");
        }

        using (RTEntities entity = new RTEntities())
        {
            int CardId;
            List<ResponseCard> card = new List<ResponseCard>();

            card = entity.ResponseCard.Where(rcard => (rcard.Enabled == true && rcard.Sac == sac)).ToList();
            if (card.Count == 0)
            {
                log.Error("[SAC:" + sac + "] Route requested for missed or blocked card");
                Response.Redirect("../?e=404");
            }
            googleurl = googleurl + HttpUtility.UrlEncode(card[0].Address)+"&saddr=";
            if (saddr != null)
            {
                googleurl=googleurl+HttpUtility.UrlEncode(saddr);
            }
            log.Info("[SAC:" + sac + "] Route has been built. Redirected to "+googleurl);
            Response.Redirect( googleurl);
                
            
        }
    }

 

}