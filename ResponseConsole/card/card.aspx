<%@ Page Language="C#" CodeFile="card.aspx.cs" Inherits="CardPage"  Debug="true" AutoEventWireup="true"%> 

<!DOCTYPE html>
<html>
<head runat="server">
	<title>Jablonet PRO :: Response Console :: Response Card</title>
	<meta name="viewport" content="width=device-width" />
	<link rel="shortcut icon" href="../favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../static/css/style.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../static/css/card.css" media="screen" />
	<script type="text/javascript" src="../static/src/jquery.js"></script>
	<script type="text/javascript">
		function success(position) {
		    var href = $('#bNavigate').attr('href');
		    $('#bNavigate').attr('href', href + position.coords.latitude + ',' + position.coords.longitude);
		    $('#gpsled').attr('class', 'ok');
			$('#gpsled').attr('title', 'Your location has been detected');
		}
		$( document ).ready(function( $ ) {
		if (navigator.geolocation)
		{
			navigator.geolocation.getCurrentPosition(success);
			
			
		}
		});
		

	</script>
</head>
<body>
<div id="wrapper">
<header runat="server">
  
<ul>
    
    <li><asp:HyperLink ID="bExit" Text="" CssClass="PanelButton" runat="server"></asp:HyperLink></li>
    <li><asp:HyperLink ID="bNavigate" Text="" CssClass="PanelButton" runat="server"></asp:HyperLink></li>
	<li id="gpsled" class="no" title="Looking for you GPS position"></li>
	
    </ul>    
      <h1 id="header">
        JABLONET PRO<br /> INTERVENTION CARD
    </h1>
</header>
<div id="content">
    <table class="unheaded">
        <tr>
            <td class="th">Synonym</td>
            <td><asp:Label id="lSynonym" runat="server" Text="" ></asp:Label></td>
        </tr>
        <tr>
            <td class="th">Name</td>
            <td><asp:Label id="lName" runat="server" Text="" ></asp:Label></td>
        </tr>
        <tr>
            <td class="th">Remarks</td>
            <td><asp:Label id="lRemark" runat="server" Text="" ></asp:Label></td>
        </tr>
        <tr>
            <td class="th">Address</td>
            <td><asp:Label id="lAddress" runat="server" Text="" ></asp:Label></td>
        </tr>
        <tr>
            <td class="th">GPS</td>
            <td><asp:Label id="GPSCoords" runat="server" Text="" ></asp:Label></td>
        </tr>
    </table>
    

        </div>
    <div id="contacts" class="pseudo-table">
    <h2>Contacts</h2>
    <div id="contacts-bg">
    <ul runat="server" id="contactslist"> </ul>
        <div style="clear:left;"></div>
    </div>
        </div>
        </div>
    <div id="pictures" class="pseudo-table">
    <h2>Pictures</h2>
        <div id="previews-bg">
        <ul runat="server" id="previews"> </ul>
            <div style="clear:left;"></div>
        </div>
        </div>
    <div id="signals" class="pseudo-table">
    <h2>Signals</h2>
        <div id="signals-bg">
    <ul runat="server" id="signalslist"> </ul>
        <div style="clear:left;"></div>
    </div>
        </div>
    
</div>

        </div>
</body>
</html>