using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using ResponseConsole;
using System.Collections;
using System.Data;
using System.IO;
using log4net;
using log4net.Config;
using System.Web.UI.HtmlControls;


public partial class CardPage : System.Web.UI.Page
{
    public static readonly ILog log = LogManager.GetLogger(typeof(CardPage));

    protected void Page_Load(object sender, EventArgs e)
    {
        log4net.Config.DOMConfigurator.Configure();

        string sac=Request["sac"];
        if (sac == null)
        {
            Response.Redirect("/");
        }
        

        Regex rgx = new Regex(@"^[a-z0-9]{8}$", RegexOptions.IgnoreCase);
        MatchCollection matches = rgx.Matches(sac);
        if (matches.Count == 0)
        {
            log.Error("[SAC:"+sac+"] Invalid request for Card");
            Response.Redirect("../?e=400");
            
        }

        using (RTEntities entity = new RTEntities())
        {
            /*
            byte[] buffer = File.ReadAllBytes("C:\\Develop\\JablonetPRO\\ResponseTeam\\ResponseConsole\\card\\demo.jpg");
            var pics = entity.Set<Picture>();
            pics.Add(new Picture { Filename="Jablik :)", Content=buffer});
            

            entity.SaveChanges();
            */
            int CardId;
            List<ResponseCard> card = new List<ResponseCard>();
            List<Signal> alarms = new List<Signal>();
            List<Contact> contacts = new List<Contact>();
            List<Picture> pictures = new List<Picture>();

            card = entity.ResponseCard.Where(rcard => (rcard.Enabled == true && rcard.Sac == sac)).ToList();
            if (card.Count == 0)
            {
                log.Error("[SAC:" + sac + "] Card not found or blocked");
                Response.Redirect("../?e=404");

            }

            lSynonym.Text = card[0].Synonym;
            lName.Text    = card[0].Name;
            lAddress.Text = card[0].Address;
            lRemark.Text    = card[0].Remark;
            GPSCoords.Text = card[0].Latitude + "," + card[0].Longitude;
            CardId=card[0].Id;

            alarms = entity.Signal.Where(al => (al.CardId == CardId )).ToList();
            CreateAlarmsGrid(alarms);
            //tAlarms.DataBind();

            contacts = entity.Contact.Where(cnt => (cnt.CardId == CardId)).ToList();
            CreateContactsGrid(contacts);
           
            

            pictures = entity.Picture.Where(pic => (pic.CardId == CardId)).ToList();
            CreatePicturesGrid(pictures, sac);
            


           
        }

        bNavigate.NavigateUrl = "../route/?sac=" + sac +"&saddr=";
        bExit.NavigateUrl = "../";
        log.Info("[SAC:" + sac + "] RC responsed");
    }

    public void CreatePicturesGrid(List<Picture> pics, string sac)
    {
        for (int i = 0; i < pics.Count; i++)
        {
            HtmlGenericControl li = new HtmlGenericControl("li");                    
            previews.Controls.Add(li);
            HtmlGenericControl anchor = new HtmlGenericControl("a");
            anchor.Attributes.Add("href", "../picture/?sac="+sac+"&pic="+pics[i].Id);
            anchor.Attributes.Add("class", "preview");
			if (pics[i].ContentType == "image/jpg")
			{
				anchor.Attributes.Add("style", "background-image:url(../picture/?sac="+sac+"&pic="+pics[i].Id+"&preview=1)");
			}
			else
			{
				anchor.Attributes.Add("style", "background-image:url(../static/img/pdf.png");
			}
            anchor.InnerText = "";
            li.Controls.Add(anchor);
        }


    }

    public void CreateContactsGrid(List<Contact> contacts)
    {
        for (int i = 0; i < contacts.Count; i++)
        {
            HtmlGenericControl li = new HtmlGenericControl("li");
            contactslist.Controls.Add(li);
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes.Add("class", "contact-name");
            div.InnerText = contacts[i].Name;
            li.Controls.Add(div);
            div = new HtmlGenericControl("div");
            div.Attributes.Add("class", "contact-position");
            div.InnerText = contacts[i].Position;
            li.Controls.Add(div);
            div = new HtmlGenericControl("div");
            div.Attributes.Add("class", "contact-phone");
            div.InnerText = contacts[i].Phone;
            li.Controls.Add(div);
        }

        
    }

    public void CreateAlarmsGrid(List<Signal> alarms)
    {
        for (int i = 0; i < alarms.Count; i++)
        {
            HtmlGenericControl li = new HtmlGenericControl("li");
            signalslist.Controls.Add(li);
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes.Add("class", "signal-description");
            div.InnerText = alarms[i].Description;
            li.Controls.Add(div);
            div = new HtmlGenericControl("div");
            div.Attributes.Add("class", "signal-timestamp");
            div.InnerText = alarms[i].OccuredAt.ToString();
            li.Controls.Add(div);
            div = new HtmlGenericControl("div");
            div.Attributes.Add("class", "signal-ext1");
            div.InnerText = alarms[i].Ext1;
            li.Controls.Add(div);
            div = new HtmlGenericControl("div");
            div.Attributes.Add("class", "signal-ext2");
            div.InnerText = alarms[i].Ext2;
            li.Controls.Add(div);
        }

        
    }

}