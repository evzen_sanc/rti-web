using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using ResponseConsole;


public partial class LoginPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["e"] != null)
            StatePanel.CssClass = "error";

        switch (Request["e"])
        {
            case "400":
                StateLabel.Text = "Invalid SMS Access code";
                break;
            case "404":
                StateLabel.Text = "Response Card not found";
                break;
        }
        
        //+DateTime.Now.ToString();
    }

}