<%@Page Language="C#" CodeFile="login.aspx.cs" Inherits="LoginPage"  Debug="true" AutoEventWireup="true"%> 

<!DOCTYPE html>
<html>
<head runat="server">
	<title>Jablonet PRO :: Response Console</title>
	<link rel="shortcut icon" href="favicon.ico" />
	<meta name="viewport" content="width=device-width" />
	<link rel="stylesheet" type="text/css" href="static/css/style.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="static/css/login.css" media="screen" />
	<script type="text/javascript" src="static/src/jquery.js"></script>
	<script type="text/javascript">
		$( document ).ready(function( $ ) {
			$("#sac").focus(function(){
				$("#sac").attr("class", "focused")
				if ($("#sac").attr("value")=="SMS Access Code")
					$("#sac").attr("value", "");
				
			})
			$("#sac").blur(function(){
				$("#sac").attr("class", "unfocused")
				if ($("#sac").attr("value")=="")
					$("#sac").attr("value", "SMS Access Code");
			})
		});
	</script>
</head>
<body>
<div id="wrapper">
<header>
	<img src="static/img/logo.png" alt="Jablonet" width="500" height="105" />
</header>
<div id="content">
	<div id="form" runat="server">

		<form method="get" action="card/" autocomplete="off">
            <asp:Panel ID="StatePanel" runat="server" CssClass="normal">
                <asp:Label id="StateLabel" runat="server"></asp:Label>
            </asp:Panel>
		<div class="form-row" >
			<div class="form-col" id="sac-field">
				<input type="text" name="sac" id="sac" class="unfocused" value="SMS Access Code" />
			</div>
			<div class="form-side">
				<button type="submit" title="Open Response Card">Open Card</button>
			</div>
		</div>
		</form>
	</div>
</div>
<footer>
	Copyright 2013&copy;
</footer>
</div>

</body>
</html>